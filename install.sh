#!/bin/bash
set -e
PROJECT_NAME="terraform-two-server"
GIT_URI="git@gitlab.com:raperrros/ansible-openvpn.git"
GIT_PROJECT="git@gitlab.com:raperrros/$PROJECT_NAME.git"
#KEY_DIR="/root/gitlabkey"
KEY_NAME="gitlab.pem"
ANSIBLE_DIR="./ansible-openvpn"
KEY_ANSIBLE_NAME="key.pem"
SERVER_NAME="two-server"
export GIT_TRACE=1
#mkdir $KEY_DIR || true # if folder already exists, continue running

#cp ./$KEY_NAME $KEY_DIR/$KEY_NAME

#chmod 600 $KEY_DIR/$KEY_NAME
chmod 600 "$KEY_NAME"

#eval "$(ssh-agent -s)"

#ssh-add $KEY_DIR/$KEY_NAME
#ssh-add "$KEY_NAME"

if [[ ! "$@" ]]; then
  echo "Error: No clients provided"
  echo 'Usage: $0 "clientname1, clientname2, ..."'
  exit 1
fi

# shellcheck disable=SC2034
export GIT_SSH_COMMAND="ssh -o PreferredAuthentications=publickey \
-o UserKnownHostsFile=/dev/null \
-o PubkeyAuthentication=yes \
-i $KEY_NAME \
-o User=git \
-o StrictHostKeyChecking=no \
-o CheckHostIP=no \
-o Hostname=gitlab.com"

git clone --verbose $GIT_PROJECT

cd "$PROJECT_NAME"/

terraform init

terraform apply

echo "wait launch instance"

sleep 60

echo "add clients: $@"

git clone "$GIT_URI"

echo "Sleeping for 5 seconds…"

sleep 5

sed -i -e 's|CLIENTS|'"$@"'|' "$ANSIBLE_DIR"/openvpn-client.yml

IP=$(cat ./terraform.tfstate | awk '/"public_ip":/{print($2)}' | awk -F '"' '{print $2}' | awk 'NR == 1')

sed -i -e 's/PUBLIC_IP/'"$IP"'/' "$ANSIBLE_DIR"/hosts.yml

sed -i -e 's/PUBLIC_IP/'"$IP"'/' "$ANSIBLE_DIR"/roles/openvpn-client/defaults/main.yml

echo -e $(grep 'private_key_pem' terraform.tfstate | sed 's|"private_key_pem": "\(.*\)",|\1|') > "$KEY_ANSIBLE_NAME"

PH=$(echo -n "$(pwd)"/;ls "$KEY_ANSIBLE_NAME");

chmod 400 ./"$KEY_ANSIBLE_NAME"

sed -i -e 's|PATH|'"$PH"'|' "$ANSIBLE_DIR"/hosts.yml

cd "$ANSIBLE_DIR"/

ansible-playbook openvpn-server.yml -l "$SERVER_NAME"

echo "Sleeping for 5 seconds…"

sleep 5

ansible-playbook openvpn-client.yml -l "$SERVER_NAME"
